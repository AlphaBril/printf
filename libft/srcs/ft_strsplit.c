/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:52:33 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/04 16:39:11 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int	ft_cntwrds(const char *s, char c)
{
	int	i;
	int	word_count;

	i = 0;
	word_count = 0;
	if (s)
	{
		while (s[i])
		{
			if (ft_isword(i, s, c))
				word_count++;
			i++;
		}
	}
	return (word_count);
}

static char	*write_word(const char *s, int wordlen, int pos)
{
	char	*word;
	int		initial_position;
	int		i;

	initial_position = pos;
	i = 0;
	if (!(word = (char*)malloc(sizeof(char) * wordlen + 1)))
		return (NULL);
	while (pos < wordlen + initial_position)
	{
		word[i] = s[pos];
		pos++;
		i++;
	}
	word[i] = '\0';
	return (word);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**r;
	int		i;
	int		j;

	if (!(r = (char**)malloc(sizeof(char*) * ft_cntwrds(s, c))) || !s || !c)
		return (NULL);
	i = 0;
	j = -1;
	while (j++ < ft_cntwrds(s, c))
	{
		while (s[i] != '\0')
		{
			if (ft_isword(i, s, c))
			{
				if (!(r[j] = (char*)malloc(sizeof(*r) * ft_wlen(s, i, c) + 1)))
					return (NULL);
				r[j++] = write_word(s, ft_wlen(s, i, c), i);
				i = i + ft_wlen(s, i, c) - (ft_wlen(s, i, c) == 1 ? 0 : 1);
			}
			else
				i++;
		}
	}
	r[--j] = 0;
	return (r);
}
