/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ldtob.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 10:24:59 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/27 11:57:18 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_ldtob(long double val)
{
	char			*ret;
	unsigned		idx;
	unsigned char	*ptr;

	ptr = (unsigned char*)&val;
	idx = 8 * sizeof(val);
	ret = NULL;
	while (idx)
	{
		ret = ptr[idx / 8] & (1u << (idx % 8))
			? ft_strjoin_free(ret, "1", 1)
			: ft_strjoin_free(ret, "0", 1);
		idx--;
	}
	return (ret);
}
