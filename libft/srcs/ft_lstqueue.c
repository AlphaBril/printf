/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstqueue.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 19:23:20 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/05 15:28:09 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_lstqueue(t_list **alst, t_list *new)
{
	t_list	*temp;

	temp = *alst;
	if (temp->content == NULL)
		*alst = ft_lstnew(new->content, new->content_size);
	else
	{
		while (temp->next)
			temp = temp->next;
		temp->next = ft_lstnew(new->content, new->content_size);
	}
}
