/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strldup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 18:36:38 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/18 12:36:49 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strldup(const char *s1, size_t n)
{
	int		i;
	char	*res;

	if (!(res = (char*)malloc(sizeof(char) * (n + 1))))
		return (0);
	i = 0;
	while (i < (int)n)
	{
		res[i] = s1[i];
		i++;
	}
	res[i] = '\0';
	return (res);
}
